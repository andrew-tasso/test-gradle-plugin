package dev.tasso.testgradleplugin.task;

public class TaskMetadata {

	private String name;
	private String group;
	private String description;

	private TaskMetadata(Builder builder) {
		setName(builder.name);
		setGroup(builder.group);
		setDescription(builder.description);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "TaskMetadata{" +
				"name='" + name + '\'' +
				", group='" + group + '\'' +
				", description='" + description + '\'' +
				'}';
	}

	/**
	 * {@code TaskMetadata} builder static inner class.
	 */
	public static final class Builder {
		private String name;
		private String group;
		private String description;

		public Builder(String theName) {

			this.name = theName;

		}

		/**
		 * Sets the {@code name} and returns a reference to this Builder so that the methods can be chained together.
		 *
		 * @param name the {@code name} to set
		 * @return a reference to this Builder
		 */
		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		/**
		 * Sets the {@code group} and returns a reference to this Builder so that the methods can be chained together.
		 *
		 * @param group the {@code group} to set
		 * @return a reference to this Builder
		 */
		public Builder withGroup(String group) {
			this.group = group;
			return this;
		}

		/**
		 * Sets the {@code description} and returns a reference to this Builder so that the methods can be chained together.
		 *
		 * @param description the {@code description} to set
		 * @return a reference to this Builder
		 */
		public Builder withDescription(String description) {
			this.description = description;
			return this;
		}

		/**
		 * Returns a {@code TaskMetadata} built from the parameters previously set.
		 *
		 * @return a {@code TaskMetadata} built with parameters of this {@code TaskMetadata.Builder}
		 */
		public TaskMetadata build() {
			return new TaskMetadata(this);
		}
	}
}
