package dev.tasso.testgradleplugin.task.initialization;

import org.gradle.api.Project;
import org.gradle.api.Task;

public interface TaskInitializer {

	void initializeTask(Project theProject);

}
