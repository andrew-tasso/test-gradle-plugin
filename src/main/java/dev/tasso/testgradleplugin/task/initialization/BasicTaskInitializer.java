package dev.tasso.testgradleplugin.task.initialization;

import dev.tasso.testgradleplugin.task.TaskMetadata;
import org.gradle.api.Project;
import org.gradle.api.Task;

public class BasicTaskInitializer<T extends Task> implements TaskInitializer {

	private TaskMetadata taskMetadata;
	private Class<T> taskClass;

	public BasicTaskInitializer(TaskMetadata theTaskMetadata, Class<T> theTaskClass) {

		this.taskMetadata = theTaskMetadata;
		this.taskClass = theTaskClass;

	}

	@Override
	public void initializeTask(Project theProject) {

		theProject.getTasks().create(this.taskMetadata.getName(), this.taskClass, this.taskMetadata);

	}
}
