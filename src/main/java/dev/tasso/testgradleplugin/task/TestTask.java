package dev.tasso.testgradleplugin.task;

import dev.tasso.testgradleplugin.task.initialization.BasicTaskInitializer;
import dev.tasso.testgradleplugin.task.initialization.TaskInitializer;
import org.gradle.api.DefaultTask;
import org.gradle.api.logging.Logger;
import org.gradle.api.logging.Logging;
import org.gradle.api.tasks.TaskAction;

import javax.inject.Inject;

public class TestTask extends DefaultTask {

	private final static Logger LOGGER = Logging.getLogger(TestTask.class);

	private static final TaskMetadata DEFAULT_META_DATA =
			new TaskMetadata.Builder("testTask")
			.withGroup("Test Group")
			.withDescription("Test Description")
			.build();

	private TaskMetadata taskMetadata;

	public TestTask() {

		this(DEFAULT_META_DATA);

	}

	@Inject
	public TestTask(TaskMetadata theTaskMetadata) {

		//Start with the default.
		this.taskMetadata = DEFAULT_META_DATA;

		//override any meta with those provided
		if(theTaskMetadata.getName() != null) {
			this.taskMetadata.setName(theTaskMetadata.getName());
		}
		if(theTaskMetadata.getGroup() != null) {
			this.taskMetadata.setGroup(theTaskMetadata.getGroup());
		}
		if(theTaskMetadata.getDescription() != null) {
			this.taskMetadata.setDescription(theTaskMetadata.getDescription());
		}

		super.setGroup(this.taskMetadata.getGroup());
		super.setDescription(this.taskMetadata.getDescription());

	}

	@TaskAction
	public void performTask() {

		LOGGER.error("I need an implementation!");

	}

	public static TaskInitializer getInitializer() {

		return getInitializer(DEFAULT_META_DATA);

	}

	public static TaskInitializer getInitializer(TaskMetadata theTaskMetadata) {

		return new BasicTaskInitializer<>(theTaskMetadata, TestTask.class);

	}

}
