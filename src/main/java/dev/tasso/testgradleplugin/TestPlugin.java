package dev.tasso.testgradleplugin;

import dev.tasso.testgradleplugin.extension.TestExtension;
import dev.tasso.testgradleplugin.task.TaskMetadata;
import dev.tasso.testgradleplugin.task.TestTask;
import dev.tasso.testgradleplugin.task.initialization.TaskInitializer;
import org.gradle.api.Incubating;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.util.ArrayList;
import java.util.List;

@Incubating
public class TestPlugin implements Plugin<Project> {

	private List<TaskInitializer> taskInitializers;

	public TestPlugin() {

		taskInitializers = new ArrayList<>();

		taskInitializers.add(TestTask.getInitializer());
		taskInitializers.add(TestTask.getInitializer(new TaskMetadata.Builder("anotherTask").build()));
		taskInitializers.add(TestTask.getInitializer(
				new TaskMetadata.Builder("yetAnotherTask").withGroup("Another").withDescription("Description!").build())
		);

	}


	@Override
	public void apply(Project project){

		project.getExtensions().create("test-gradle-plugin", TestExtension.class);

		taskInitializers.forEach(taskInitializer -> taskInitializer.initializeTask(project));

	}

}
