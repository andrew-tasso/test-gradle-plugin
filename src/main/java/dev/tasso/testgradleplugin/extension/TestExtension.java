package dev.tasso.testgradleplugin.extension;

public class TestExtension {

	private String firstValue;
	private String secondValue;

	public String getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(String firstValue) {
		this.firstValue = firstValue;
	}

	public String getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(String secondValue) {
		this.secondValue = secondValue;
	}

	@Override
	public String toString() {
		return "TestExtension{" +
				"firstValue='" + firstValue + '\'' +
				", secondValue='" + secondValue + '\'' +
				'}';
	}
}
